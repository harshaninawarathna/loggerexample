﻿using log4net;

namespace SampleLogger
{
    internal class Program
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Program));

        private static void Main()
        {
            Logger.Info("Logger works!");
        }
    }
}
